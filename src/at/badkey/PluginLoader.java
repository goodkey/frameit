package at.badkey;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import at.badkey.frameit.api.FrameItPlugin;
import at.badkey.frameit.main.FrameItServer;

public class PluginLoader {
	
	public void loadPlugins()  {
		File dir = new File("C:\\plugin");
		if(!dir.isDirectory()) return;
		for(File f : dir.listFiles()) {
			try {
				loadPlugin(f);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void loadPlugin(File f) throws ClassNotFoundException, IOException {
		JarFile jarFile = new JarFile(f);
		Enumeration<JarEntry> e = jarFile.entries();

		URL[] urls = { new URL("jar:file:" + f.getAbsolutePath() +"!/") };
		URLClassLoader cl = URLClassLoader.newInstance(urls);
		
		while (e.hasMoreElements()) {
		    JarEntry je = e.nextElement();
		    if(je.isDirectory() || !je.getName().endsWith(".class")){
		        continue;
		    }
		    // -6 because of .class
		    String className = je.getName().substring(0,je.getName().length()-6);
		    className = className.replace('/', '.');
		    Class<?> c = cl.loadClass(className);
		    
		    if(c.getSuperclass() != FrameItPlugin.class) {
		    	continue;
		    }
		    
		    	FrameItPlugin obj = null;
			    try {
			    	obj = (FrameItPlugin) c.newInstance();
				} catch (SecurityException | InstantiationException | IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					continue;
				}
			    FrameItServer.registerPlugin(obj.getName(), obj);
		}
		jarFile.close();
	}
}
