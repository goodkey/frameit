package at.badkey.frameit.exceptions;

public class CommandNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7407767470063808234L;

	public CommandNotFoundException() {
		super("Command not found!");
	}
	
}
