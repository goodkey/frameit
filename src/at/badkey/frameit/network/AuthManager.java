package at.badkey.frameit.network;

import java.net.DatagramPacket;
import java.util.ArrayList;

public class AuthManager {

	public ArrayList<NetObject> openConnections = new ArrayList<NetObject>();
	
	/**
	 * 
	 * @param socket
	 * @return
	 */
	public NetObject indentify(DatagramPacket socket) {
		for(NetObject p : openConnections) {
			if(!p.getIp().getHostAddress().equals(socket.getAddress().getHostAddress())) continue;
			if(p.getGamePort() != socket.getPort() && p.getPort() != socket.getPort()) continue;
			p.setLastMessageTimeStamp(System.currentTimeMillis());
			return p;
		}
		
		return null;
	}
	
}
