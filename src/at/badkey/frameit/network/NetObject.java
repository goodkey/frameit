package at.badkey.frameit.network;

import java.net.InetAddress;

public class NetObject {
	
	private InetAddress ip;
	private int uid;
	private int port,gamePort;
	
	private long lastMessageTimeStamp = System.currentTimeMillis();
	
	/**
	 * 
	 * @param ip
	 * @param port
	 * @param gamePort
	 * @param uid
	 */
	public NetObject(InetAddress ip, int port, int gamePort,int uid) {
		this.ip = ip;
		this.uid = uid;
		this.port = port;
		this.gamePort = gamePort;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getLastMessageTimeStamp() {
		return lastMessageTimeStamp;
	}
	
	/**
	 * 
	 * @param lastMessageTimeStamp
	 */
	public void setLastMessageTimeStamp(long lastMessageTimeStamp) {
		this.lastMessageTimeStamp = lastMessageTimeStamp;
	}
	
	/**
	 * Returns player's IP.
	 * @return
	 */
	public InetAddress getIp() {
		return ip;
	}
	
	/**
	 * Returns player's game port.
	 * @return
	 */
	public int getGamePort() {
		return gamePort;
	}
	
	/**
	 * Returns the unique ID of the player.
	 * @return
	 */
	public int getUid() {
		return uid;
	}
	
	/**
	 * Returns player's communication port.
	 * @return
	 */
	public int getPort() {
		return port;
	}

}
