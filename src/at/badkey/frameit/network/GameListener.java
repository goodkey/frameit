package at.badkey.frameit.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import at.badkey.frameit.main.Console;
import at.badkey.frameit.main.FrameItServer;
import at.badkey.frameit.network.handling.NetObjectConnectionHandler;

public class GameListener extends Thread{

	DatagramSocket connection;
	int port = 6501;

	public GameListener() {
		try {
			this.connection = new DatagramSocket(port);
		} catch (IOException e) {
			System.out.println(Console.getCurrentTimeStamp() + "Error binding port " + port);
			e.printStackTrace();
			return;
		}
		System.out.println(Console.getCurrentTimeStamp()  + "Started listening for game connections...");
	}
	
	@Override
	public void run() {
		while (true) {
			byte[] buffer = new byte[256];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			try {
				connection.receive(packet);
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			
			NetObject p = FrameItServer.authManager.indentify(packet);
			
			if(p == null) continue;
			
			NetObjectConnectionHandler handler = new NetObjectConnectionHandler(connection, packet, p);
			System.out.println(Console.getCurrentTimeStamp() + p.getUid() + " => " + handler.readBuffer());
			String request = handler.readBuffer();
			FrameItServer.commandManager.executeCommand(request, handler);
		}
	}
	
}
