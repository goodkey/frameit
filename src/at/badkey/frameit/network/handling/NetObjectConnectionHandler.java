package at.badkey.frameit.network.handling;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import at.badkey.frameit.network.NetObject;

public class NetObjectConnectionHandler extends ConnectionHandler{

	private NetObject player;
	
	public NetObjectConnectionHandler(DatagramSocket socket, DatagramPacket packet, NetObject p) {
		super(socket, packet);
		this.player = p;
	}
	
	public NetObject getNetObject() {
		return player;
	}

}
