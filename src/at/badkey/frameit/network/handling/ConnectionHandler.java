package at.badkey.frameit.network.handling;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ConnectionHandler {

	private DatagramSocket socket;
	private DatagramPacket packet;
	
	public ConnectionHandler(DatagramSocket socket, DatagramPacket packet) {
		this.packet = packet;this.socket = socket;
	}
	
	protected DatagramSocket getSocket() {
		return socket;
	}
	
	public DatagramPacket getPacket() {
		return packet;
	}
	
	public void send(String str) throws IOException {
		DatagramPacket packet = new DatagramPacket(str.getBytes(), 0, str.getBytes().length, getPacket().getAddress(), getPacket().getPort());
		getSocket().send(packet);
	}
	
	public String retrieve() throws IOException {
		byte[] buffer = new byte[256];
		DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length, getPacket().getAddress(), getPacket().getPort());
		getSocket().receive(packet);
		
		return readBuffer(packet);
	}
	
	public String readBuffer() {
		return readBuffer(getPacket());
	}
	
	public String readBuffer(DatagramPacket packet) {
		return new String(packet.getData(), 0, packet.getLength());
	}
	
}
