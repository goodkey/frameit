package at.badkey.frameit.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import at.badkey.frameit.main.Console;
import at.badkey.frameit.main.FrameItServer;
import at.badkey.frameit.network.handling.ConnectionHandler;
import at.badkey.frameit.network.handling.NetObjectConnectionHandler;

public class SocketListener extends Thread {

	DatagramSocket connection;
	int port = 6500;
	byte[] buffer = new byte[256];
	DatagramPacket packet;
	int uids = 0;

	public SocketListener() {
		try {
			this.connection = new DatagramSocket(port);
		} catch (IOException e) {
			System.out.println(Console.getCurrentTimeStamp() + "Error binding port " + port);
			e.printStackTrace();
			return;
		}
		System.out.println(Console.getCurrentTimeStamp() + "Started listening for connections...");
	}

	private void registerPlayer(ConnectionHandler handler) {
		String request = handler.readBuffer();
		
		if(request.toLowerCase().startsWith("userid ")) {
			try {
				int gamePort = Integer.parseInt(request.replaceFirst("userid ", ""));
				
				uids ++;
				
				NetObject p = new NetObject(handler.getPacket().getAddress(), handler.getPacket().getPort(), gamePort, uids);
				
				FrameItServer.authManager.openConnections.add(p);
				
				try {
					handler.send(uids + "");
				} catch (IOException e) {
				}
			}catch(NumberFormatException e) {
				try {
					handler.send("error wrongusage");
				} catch (IOException e1) {
				}
			}
		}else {
			try {
				handler.send("error autherror");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void run() {
		while (true) {
			this.packet = new DatagramPacket(buffer, buffer.length);
			try {
				connection.receive(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			NetObject p = FrameItServer.authManager.indentify(packet);
			
			if(p == null) {
				//Player not found -> Has to log in!
				
				ConnectionHandler handler = new ConnectionHandler(connection, packet);
				registerPlayer(handler);
			}else {
				//Player found -> Can continue!
				NetObjectConnectionHandler handler = new NetObjectConnectionHandler(connection, packet, p);
				String request = handler.readBuffer();
				
				FrameItServer.commandManager.executeCommand(request, handler);
			}
		}
	}

}
