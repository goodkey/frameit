package at.badkey.frameit.commands;

import java.net.DatagramPacket;

import at.badkey.frameit.api.commands.CommandExecutor;
import at.badkey.frameit.main.Console;
import at.badkey.frameit.network.NetObject;
import at.badkey.frameit.network.handling.NetObjectConnectionHandler;

public class KeepAliveCommand implements CommandExecutor{

	@Override
	public void onCommand(NetObjectConnectionHandler executor, String[] args, String label) {
		DatagramPacket packet = executor.getPacket();
		NetObject p = executor.getNetObject();
		System.out.println(Console.getCurrentTimeStamp() + "Received keep alive from " + packet.getAddress().getHostAddress() + ":" + packet.getPort() + " (Player: " + p.getUid() + ")");
	}

}
