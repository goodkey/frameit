package at.badkey.frameit.main;

import java.io.IOException;

import at.badkey.PluginLoader;
import at.badkey.frameit.api.FrameItPlugin;
import at.badkey.frameit.api.commands.CommandManager;
import at.badkey.frameit.network.AuthManager;
import at.badkey.frameit.network.GameListener;
import at.badkey.frameit.network.SocketListener;


public class FrameItServer {

	public static AuthManager authManager = new AuthManager();
	public static CommandManager commandManager = CommandManager.getInstance();

	/**
	 * Startup server.
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		new Console();
		
		//Load plugins
		PluginLoader loader = new PluginLoader();
		loader.loadPlugins();
		
		Thread connections = new SocketListener();
		connections.setPriority(2);
		connections.start();
		
		Thread gameConnections = new GameListener();
		gameConnections.setPriority(3);
		gameConnections.start();
	}
	
	public static void registerPlugin(String name, FrameItPlugin pl) {
		try {
			pl.onEnabled();
		}catch (Exception e) {
			System.out.println("Could not load plugin "+name);
		}
	}

}
