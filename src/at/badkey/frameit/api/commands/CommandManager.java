package at.badkey.frameit.api.commands;

import java.util.ArrayList;
import java.util.Arrays;

import at.badkey.frameit.commands.KeepAliveCommand;
import at.badkey.frameit.exceptions.CommandNotFoundException;
import at.badkey.frameit.network.handling.NetObjectConnectionHandler;

public class CommandManager {
	
	public static CommandManager instance;
	
	public ArrayList<Command> commands = new ArrayList<Command>();
	
	public static CommandManager getInstance() {
		if(instance == null) {
			instance = new CommandManager();
			instance.registerPreDefinedCommands();
		}
		return instance;
	}

	public void registerCommand(String command, CommandExecutor executor) {
		commands.add(new Command(command, executor));
	}
	
	public void executeCommand(String command, NetObjectConnectionHandler executor) {
		for(Command cmd : commands) {
			ArrayList<String> arguments = new ArrayList<String>(Arrays.asList(command.split(" ")));
			command = arguments.get(0);
			arguments.remove(0);
			System.out.println(command);
			if(cmd.getCommand().equalsIgnoreCase(command)) {
				cmd.execute(command, arguments.toArray(new String[0]),executor);
				return;
			}
		}
		throw new CommandNotFoundException();
	}
	
	private void registerPreDefinedCommands() {
		registerCommand("keepAlive", new KeepAliveCommand());
	}

}
