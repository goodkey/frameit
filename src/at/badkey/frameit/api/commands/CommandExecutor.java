package at.badkey.frameit.api.commands;

import at.badkey.frameit.network.handling.NetObjectConnectionHandler;

public interface CommandExecutor {
	public void onCommand(NetObjectConnectionHandler executor, String[] args, String label);
}
