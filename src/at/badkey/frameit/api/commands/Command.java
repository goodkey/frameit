package at.badkey.frameit.api.commands;

import at.badkey.frameit.network.handling.NetObjectConnectionHandler;

public class Command {
	private String command;
	private CommandExecutor instance;
	
	public Command(String command,CommandExecutor instance) {
		this.command = command;
		this.instance = instance;
	}
	
	public void execute(String command, String[] args, NetObjectConnectionHandler executor) {
		instance.onCommand(executor, args, command);
	}
	
	public String getCommand() {
		return command;
	}

}
