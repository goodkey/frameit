package at.badkey.frameit.api;

public abstract class FrameItPlugin {
	
	public void onEnabled() {
		System.out.println("Extension "+getName()+" enabled.");
	}
	
	public void onDisabled() {
		System.out.println("Extension "+getName()+" disabled.");
	}
	
	public abstract String getName();

}
